
//#region Generale

var i;

// $(document).ready(function() {
//     i = 1;
//     $('#header').css('background-image', 'url("content/img/drone/1.jpg")');
//     setInterval(cambiasfondo, 3000);
// });

function cambiasfondo() {
    i++;
    if (i == 4)
        i = 1;
    $('#header').css('background-image', 'url("content/img/drone/' + i.toString() + '.jpg")');
}

$('#butInformazioni').click(function() {
    window.location.href='#divInfo';
});

$('#myNavbar > ul > li > a').click(function() {
    if ($(window).width() <= 768) {  
        $('#hamburger').trigger('click');
    }     
});

$('.getstart').click(function() {
    $('.divAttivitaDettaglio').hide();
    $('.divAttivitaDettaglio').eq($('.getstart').index($(this))).show();

    // $('#modalAttivitaContenuto').animate({
    //     scrollTop: $('#modalAttivitaContenuto').offset().top
    // });

    $('body').css('overflow', 'hidden');
    $('#divModalAttivita').show();
});

$('#butModalAttivitaAnnulla').click(function() {
    $('body').css('overflow', 'auto');
    $('#divModalAttivita').hide();
});

$('.butApriPrenota').click(function() {
    $('body').css('overflow', 'hidden');

    //avviso('CI VEDIAMO A MAGGIO', 'Ciao! Piacerebbe molto anche noi poter praticare rafting tutto l\'anno.<br>Purtroppo le condizioni del fiume e del meteo non permettono di godersi appieno questo tipo di attivit&agrave; durante la stagione invernale.</br>Ma non preoccuparti, riapriremo il servizio di prenotazione in <b>aprile</b>, per tornare a divertirci da inizio <b>maggio</b>!');
    // quando tornerà disponibile la prenotazione
    $('#divModalPrenota').show();
});

$('#butModalPrenotaAnnulla').click(function() {
    $('body').css('overflow', 'auto');
    $('#divModalPrenota').hide();
});

$('#butPerchèOSM').click(function() {
    $('body').css('overflow', 'hidden');
    $('#divModalOSM').show();
});

$('#butModalOSMAnnulla').click(function() {
    $('body').css('overflow', 'auto');
    $('#divModalOSM').hide();
});

$('#butModalAvvisonnulla').click(function() {
    $('body').css('overflow', 'auto');
    $('#divModalAvviso').hide();
});

function avviso(t, d) {
    $('#pTitoloAvviso').text(t);
    $('#pDescrizioneAvviso').html(d);
    $('body').css('overflow', 'hidden');
    $('#divModalAvviso').show();
}

/* Calendario */
$("#inpData").datepicker({
    showButtonPanel: true,
    dateFormat: "dd/mm/yyyy",
    changeMonth: true,
    changeYear: false,
    yearRange: "c-100:c+10",
    dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
    buttonImageOnly: true,
    buttonImage: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAATCAYAAAB2pebxAAABGUlEQVQ4jc2UP06EQBjFfyCN3ZR2yxHwBGBCYUIhN1hqGrWj03KsiM3Y7p7AI8CeQI/ATbBgiE+gMlvsS8jM+97jy5s/mQCFszFQAQN1c2AJZzMgA3rqpgcYx5FQDAb4Ah6AFmdfNxp0QAp0OJvMUii2BDDUzS3w7s2KOcGd5+UsRDhbAo+AWfyU4GwnPAYG4XucTYOPt1PkG2SsYTbq2iT2X3ZFkVeeTChyA9wDN5uNi/x62TzaMD5t1DTdy7rsbPfnJNan0i24ejOcHUPOgLM0CSTuyY+pzAH2wFG46jugupw9mZczSORl/BZ4Fq56ArTzPYn5vUA6h/XNVX03DZe0J59Maxsk7iCeBPgWrroB4sA/LiX/R/8DOHhi5y8Apx4AAAAASUVORK5CYII=",
    buttonText: "Seleziona data",
    showOn: "button",
    hideIfNoPrevNext: true,
});

$('#butModalPrenotaPrenota').click(function() {
    var dati = {};

    if ($('#inpNominativo').val() == '') {
        avviso('Attenzione', 'Inserisci un nominativo corretto');
        $('#butModalAvvisonnulla').mouseup(function () {
            $('#inpNominativo').trigger('focus'); 
            $('#butModalAvvisonnulla').off('mouseup');
        });
        return;
    }
    dati['nominativo'] = $('#inpNominativo').val();

    if ($('#inpMail').val() == '') {
        avviso('Attenzione', 'Inserisci una mail corretta');
        $('#butModalAvvisonnulla').mouseup(function () {
            $('#inpMail').trigger('focus'); 
            $('#butModalAvvisonnulla').off('mouseup');
        });
        return;
    }
    dati['mail'] = $('#inpMail').val();

    dati['recapito'] = $('#inpRecapito').val();
    dati['attività'] = $('#selectAttivita').children('option:selected').val();
    dati['partecipanti'] = $('#inpPartecipanti').val();
    dati['data'] = $('#inpData').val();
    dati['turno'] = $("input[name='radioTurno']:checked").val();
    dati['note'] = $('#inpNote').val();

    $.ajax({
        url: 'https://formspree.io/xgenyyje',
        method: 'POST',
        data: dati,
        dataType: 'json'
    });
    avviso('Prenotazione effettuata!', 'Ti contatteremo al pi&ugrave; presto.');
    $('#butModalAvvisonnulla').mouseup(function () {
        $('#divModalPrenota').hide();
        $('#butModalAvvisonnulla').off('mouseup');
    });
});


//#endregion


//#region Leaflet

var mappa = L.map(
    'divLeaflet',
    {
        center: [45.6221769, 9.5210785],
        zoom: 14,
        zoomControl: true,
        layers: [
            L.tileLayer(
                'https://{s}.tile.osm.org/{z}/{x}/{y}.png',
                {
                    maxZoom: 18,
                    maxNativeZoom: 18,
                    attribution: '&copy; <a href="https://www.openstreetmap.org/" target="_blank">OpenStreetMap</a>',
                }
            )
        ]
    }
);

var icona = L.icon({
    iconUrl: 'content/img/indicatore/indicatore.png',
    shadowUrl: 'content/img/indicatore/ombra.png',
    iconSize: [100, 120],
    iconAnchor: [50, 120],
    shadowSize:   [180, 120],
    shadowAnchor: [54, 120],
    popupAnchor: [0, -120]
});

var indicatore = L.marker(
    [45.6221769, 9.5210785],
    {
        icon: icona,
        draggable: false,
        title: '',
    }
)
.addTo(mappa)
.bindPopup('<b>Compagnia del Biscotto</b><br>Via Gandolfi<br>24042 Capriate San Gervasio (BG)')
.openPopup();


//#endregion
